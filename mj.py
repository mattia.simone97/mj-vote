#!/usr/bin/env python3
# coding: utf-8

import sys
import csv
import math
from operator import itemgetter

CANDIDATES = {}

MENTIONS = [
    "Pessimo",
    "Insufficiente",
    "Mediocre",
    "Accettabile",
    "Buono",
    "Ottimo",
    "Eccellente"
]

final_ranking = []


def results_hash(votes):
    #Inizializza vettore giudizi per ogni candidato
    candidates_results = {
                candidate: [0]*len(MENTIONS)
                for candidate in CANDIDATES.keys()
    }
    #Crea la matrice dei voti candidato numero di giudizi per grado
    for vote in votes:
        for candidate, mention in vote.items():
            candidates_results[candidate][mention] += 1

    return candidates_results


def majority_median_result(candidate_results, median):
    r = []
    for candidate, candidate_result in candidate_results.items():
        vote_counter = 0
        i = 0
        for vote_count_by_mention in candidate_result:
            vote_counter = vote_counter + vote_count_by_mention
            if median < vote_counter:
                element  = {
                    "candidate": candidate,
                    "median_mention": i,
                    "candidate_results": candidate_result
                }
                r.append(element)
                break
            i = i + 1
    return r


def sort_candidates(median_results):
    sorted_list = sorted(median_results, reverse=True, key=itemgetter('median_mention'))
    return sorted_list


def tie_break(results, vote_number):
    candidates_to_tie_break = []
    highest_median_mention = results[0]["median_mention"]
    for result in results:
        if result["median_mention"] == highest_median_mention:
            candidates_to_tie_break.append(result)
        else:
            final_ranking.append(result)

    if len(candidates_to_tie_break) > 1 and highest_median_mention > 0:
        new_results = {}
        vote_number -= 1
        for candidate in candidates_to_tie_break:
            candidate["candidate_results"][highest_median_mention] -= 1
            new_results[candidate["candidate"]] = candidate["candidate_results"]
        median = math.ceil(vote_number/2)
        median_res = majority_median_result(new_results, median)
        sorted_res = sort_candidates(median_res)
        tie_break(sorted_res, vote_number)
    else:
        for candidate in candidates_to_tie_break:
            final_ranking.append(candidate)


def get_full_rank(number_votes):
    if len(final_ranking) > 0:
        print(final_ranking)
        tie_break(final_ranking, number_votes)
        candidate = final_ranking[len(final_ranking)-1]
        print("candidato  " + CANDIDATES[candidate["candidate"]] + " con giudizio " + MENTIONS[candidate["median_mention"]])
        get_full_rank(number_votes)
    else:
        return


def main(filename):
    votes = []

    with open(filename) as results_file:
        reader = csv.reader(results_file, delimiter=',')
        candidate_list = next(reader)
        id_counter = 1
        for candidate in candidate_list:
            CANDIDATES[str(id_counter)] = candidate
            id_counter = id_counter + 1

        for row in reader:
            vote = {}
            counter = 1
            for judgment in row:
                vote[str(counter)] = MENTIONS.index(judgment)
                counter = counter + 1
            votes.append(vote)
    number_votes = len(votes)
    median = math.ceil(number_votes/2)
    results = results_hash(votes)
    median_results = majority_median_result(results, median)
    sorted_results_by_median = sort_candidates(median_results)
    tie_break(sorted_results_by_median, number_votes)
    winner = final_ranking[len(final_ranking)-1]
    print("Vincitore: " + CANDIDATES[winner["candidate"]] + " con giudizio " + MENTIONS[winner["median_mention"]])
    #get_full_rank(number_votes)


if __name__ == '__main__':
    main(sys.argv[1])
