#!/usr/bin/env python3
# coding: utf-8

import csv
import os
import datetime

this_path = os.path.abspath(__file__)

results_raw_path = os.path.dirname(this_path) + "\\results_raw\\"
results_checked = os.path.dirname(this_path) + "\\results_checked\\"
voter_rolls = os.path.dirname(this_path) + "\\voter_rolls\\"


class ParsedRow:
    def __init__(self, timestamp, votes, token):
        self.timestamp = timestamp
        self.votes = votes
        self.token = token


def load_token_from_roll(filename):
    tokens = []
    with open(filename) as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader)
        for row in reader:
            tokens.append(row[0])

    return tokens


def check_token(token, token_roll):
    if token in token_roll:
        return 1
    return 0


def parse_timestamp(date):
    date_time_obj = datetime.datetime.strptime(date, '%m/%d/%Y %H:%M:%S')
    return date_time_obj.timestamp()


def check_votes(filename, token_roll):
    results = []
    line_parsed = 0
    with open(filename) as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        parsed_header = next(reader)
        column_number = len(parsed_header)
        start_votes_column = 1
        end_votes_column = column_number - 1

        timestamp = parsed_header[0]
        votes = parsed_header[start_votes_column:end_votes_column]
        token = parsed_header[column_number-1]
        results.append(ParsedRow(timestamp, votes, token))

        for row in reader:
            line_parsed = line_parsed + 1
            timestamp = parse_timestamp(row[0])
            votes = row[start_votes_column:end_votes_column]
            token = row[column_number-1]
            if (check_token(token, token_roll)):
                results.append(ParsedRow(timestamp, votes, token))
    print("Voti raccolti: " + str(line_parsed))
    return results


def check_double_votes(votes):
    double_votes_check_result = []
    seen = set()
    header = votes[0]
    votes_without_header = checked_votes[1:len(votes)]
    votes_without_header.sort(reverse=True, key=lambda x: x.timestamp)
    double_votes_check_result.append(header)
    for vote in votes_without_header:
        if vote.token not in seen:
            double_votes_check_result.append(vote)
            seen.add(vote.token)
    return double_votes_check_result


def write_results_to_csv(results, filename):
    with open(filename, mode='w', newline='') as csv_file:
        writer = csv.writer(
            csv_file, delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL
        )
        for vote in results:
            writer.writerow(vote.votes)


for filename in os.listdir(results_raw_path):
    tokens = load_token_from_roll(voter_rolls + filename)
    checked_votes = check_votes(results_raw_path + filename, set(tokens))
    checked_double_votes = check_double_votes(checked_votes)
    print("Voti validi: " + str(len(checked_double_votes)-1))
    write_results_to_csv(checked_double_votes, results_checked + filename)
    print("DONE")
