#!/usr/bin/env python3
# coding: utf-8

import os
import csv
import smtplib
import ssl
import time


from string import Template
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

VOTE_ID = {
    'nord-ovest.csv': '5',
    'nord-est.csv': '6',
    'centro.csv': '7',
    'sud.csv': '8',
    'isole.csv': '9'
}

this_path = os.path.abspath(__file__)
voter_lists_with_tokens = os.path.dirname(
    this_path) + "\\voter_lists_with_tokens\\"

context = ssl.create_default_context()
email_server = smtplib.SMTP(
    host='email-smtp.eu-west-1.amazonaws.com',
    port=587)
email_server.starttls(context=context)
email_server.login(
    'AKIAV57MFZEZ6K7VHWZR',
    'BHq0b2TfvIWBc1GxDA8fLoqJWTenusm6IGBjKfTKCdq9')

file = open("template.html", "r")
template = Template(file.read())


def send_email_to_voter(full_name, email, token, vote_id):
    msg = MIMEMultipart()
    body = template.substitute(
        FULL_NAME=full_name,
        TOKEN=token,
        VOTE_ID=vote_id)
    msg['From'] = 'Volt Italia <volt-it@ses.volteuropa.org>'
    msg['To'] = email
    msg['Subject'] = 'Consultazione Comunali Trento 2020 Volt Trento'

    msg.attach(MIMEText(body, "html"))

    try:
        email_server.send_message(msg)
        print(full_name + ' - ' + email)
    except Exception as e:
        print(e)
        return False
    return True


def load_voters_from_list_and_send_emails(path, vote_id):
    voters_with_errors = []
    with open(path) as csv_file:
        reader = csv.reader(csv_file, delimiter=',')

        next(reader)
        i = 0
        for row in reader:
            i += 1
            res = send_email_to_voter(
                row[0] + ' ' + row[1],
                row[2],
                row[3],
                vote_id
            )

            if ((i % 25) == 0):
                time.sleep(2)

            if not res:
                voters_with_errors.append(row)
    return voters_with_errors

for filename in os.listdir(voter_lists_with_tokens):
    voters_with_errors = load_voters_from_list_and_send_emails(
        voter_lists_with_tokens + filename,
        1
        )
    if len(voters_with_errors) == 0:
        print("DONE")
    else:
        print(voters_with_errors)
        print('BLASTER: ' + len(voters_with_errors) + ' errori')
