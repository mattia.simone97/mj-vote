#!/usr/bin/env python3
# coding: utf-8

import os
import csv
import jwt


this_path = os.path.abspath(__file__)
voter_lists = os.path.dirname(this_path) + "\\voter_lists\\"
voter_lists_with_tokens = os.path.dirname(this_path) + "\\voter_lists_with_tokens\\"

secret = '3r9ub384fb340b0b4b04g430gb0384g0b084083g80b4gb08340g0b4gb0n49g347gb3b9g9gb4b08gbg340b8b0w844inifnenbfevnqoq'


class Voter:
    def __init__(self, first_name, last_name, email):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        encoded_token = jwt.encode({'email': email}, secret, algorithm='HS256')
        self.token = encoded_token.decode('UTF-8')

    def get_serialized_data(self):
        serialezed_data = []
        serialezed_data.append(self.first_name)
        serialezed_data.append(self.last_name)
        serialezed_data.append(self.email)
        serialezed_data.append(self.token)
        return serialezed_data


def write_results_to_csv(voter_list, path):
    with open(path, mode='w', newline='') as csv_file:
        writer = csv.writer(
            csv_file, delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL
        )
        for voter in voter_list:
            writer.writerow(voter)


def load_voters_from_list(path):
    voter_list_with_tokens = []
    with open(path) as csv_file:
        reader = csv.reader(csv_file, delimiter=',')

        header = next(reader)
        header.append('token')
        voter_list_with_tokens.append(header)

        for row in reader:
            voter = Voter(row[0], row[1], row[2])
            print(voter.token)
            voter_list_with_tokens.append(voter.get_serialized_data())

    return voter_list_with_tokens


for filename in os.listdir(voter_lists):
    voter_list_with_tokens = load_voters_from_list(voter_lists + filename)
    write_results_to_csv(
        voter_list_with_tokens,
        voter_lists_with_tokens + filename)
    print("DONE")
